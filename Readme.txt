/**
 * CS1555 Term Project - myAuction (Group 5)
 * by Raymond Wang (Gengyu), Ben Isenberg
 * gew11@pitt.edu, bji6@pitt.edu
 * 
 * Copyright (C) 2013, All rights reserved.
 * 
 */

Using prepared statements for user input to make SQL injection an impossibility.