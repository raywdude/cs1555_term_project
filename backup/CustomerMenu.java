/**
 * CS1555 Term Project - myAuction (Group 5)
 * by Raymond Wang (Gengyu), Ben Isenberg
 * gew11@pitt.edu, bji6@pitt.edu
 * 
 * Copyright (C) 2013, All rights reserved.
 * 
 *	Redistribution and use in source and binary forms, with or without
 *	modification, are permitted provided that the following conditions are met:
 *	    * Redistributions of source code must retain the above copyright
 *	      notice, this list of conditions and the following disclaimer.
 *	    * Redistributions in binary form must reproduce the above copyright
 *	      notice, this list of conditions and the following disclaimer in the
 *	      documentation and/or other materials provided with the distribution.
 *	    * Neither the name of the <organization> nor the
 *	      names of its contributors may be used to endorse or promote products
 *	      derived from this software without specific prior written permission.
 *	
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *	DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;


public class CustomerMenu {
	 private Connection connection; //used to hold the jdbc connection to the DB
	 private String username, password;
	 
	 private MainMenu mm;
	 
	 public CustomerMenu(MainMenu mm, Connection c, String u, String p) {
		 username = u;
		 password = p;
		 connection = c;
		 this.mm = mm;
	 }
	 
	 public void doCustomerMenu(Scanner sc) {
		 int option = 0;
		 do {
			 // display menu
			 System.out.println("------------CUSTOMER MENU-----------");
			 System.out.println("1: Browse products");
			 System.out.println("2: Search for products");
			 System.out.println("3: Put a product up for auction");
			 System.out.println("4: Bid on a product");
			 System.out.println("5: Sell product");
			 System.out.println("6: Product suggestions");
			 System.out.println("0: Logout and quit session");
			 System.out.println("------------------------------------");
			 
			 // prompt user for input
			 System.out.print("Enter your option: ");
			 option = sc.nextInt(); sc.nextLine();
			 
			 switch(option) {
			 case 1:
				 // browse products
				 doBrowseProducts(sc);
				 break;
			 case 2:
				 // search for products
				 doSearchProducts(sc);
				 break;
			 case 3:
				 // put product up for auction
				 doPutProduct(sc);
				 break;
			 case 4:
				 // bid on product
				 doBidProduct(sc);
				 break;
			 case 5:
				 // sell product
			 	 sellProduct(sc);
				 break;
			 case 6:
				 // show suggestions
				 doShowSuggestions();
				 break;
			 default: // everything else... ignore
				 break;
			 }
		 }
		 while(option != 0);
	 }
	 
	 public void doBidProduct(Scanner sc) {
		 try {
			 int auctionid = 0;
			 int amount = 0;
			 int curamount = 0;
			 System.out.print("Enter the auction id to bid on: ");
			 auctionid = sc.nextInt(); sc.nextLine();
			 PreparedStatement prep = connection.prepareStatement("select amount from product where auction_id=? and status='underauction'");
			 prep.setInt(1, auctionid);
			 ResultSet rs = prep.executeQuery();
			 if(rs.next()) {
				 curamount = rs.getInt(1);
				 do {
					 System.out.print("Enter the amount of money to bid on (must be higher than "+curamount+"): ");
					 amount = sc.nextInt(); sc.nextLine();
					 if(amount <= curamount) {
						 System.out.println("Error: Please enter an amount greater than " + curamount);
					 }
				 } while(amount <= curamount);
				 
				 prep = connection.prepareStatement("select max(bidsn) from bidlog");
				 rs = prep.executeQuery();
				 if(rs.next()) {
					 int bidsn = rs.getInt(1) + 1;
					 //     BIDSN AUCTION_ID BIDDER     BID_TIME      AMOUNT
					 rs = mm.doQuery("select C_Date from OURDATE");
					 if(rs.next()) {
						 Date date = rs.getDate(1);
						 prep = connection.prepareStatement("insert into bidlog values(?,?,?,?,?)");
						 prep.setInt(1, bidsn);
						 prep.setInt(2, auctionid);
						 prep.setString(3, username);
						 prep.setDate(4, date);
						 prep.setInt(5, amount);
						 prep.executeUpdate();
						 
						 // must remember to commit!
						 connection.commit();
						 System.out.println("Your bid of $" + amount + " on auction id " + auctionid + " was successful!");
					 }
					 else {
						 System.out.println("Error: Unable to obtain system date and time.");
					 }
				 }
				 else {
					 System.out.println("Error: Unknown system error occured.");
				 }
				 
			 }
			 else {
				 System.out.println("Error: Auction not found or already ended.");
			 }
		 }
		 catch(Exception e){
			 System.out.println("Error: " + e);
		 }
	 }
	 
	 public void doShowSuggestions() {
/*
(select * from product p2,bidlog bl2 where p2.auction_id=bl2.auction_id and
bl2.bidder in (select bl.bidder from product p,bidlog bl where p.auction_id=bl.auction_id and bl.bidder<>'user3' and
bl.auction_id in (select auction_id from bidlog where bidder='user3')) and p2.status='underauction')
MINUS
(select * from product p,bidlog bl where p.auction_id=bl.auction_id and bl.bidder='user3');
 */
		 try {
			String query = "select * from ((select p2.auction_id,p2.name,p2.description,p2.amount from product p2,bidlog bl2 where p2.auction_id=bl2.auction_id and\n" + 
"bl2.bidder in (select bl.bidder from product p,bidlog bl where p.auction_id=bl.auction_id and bl.bidder<>? and\n" +
"bl.auction_id in (select auction_id from bidlog where bidder=?)) and p2.status='underauction')\n"+
"MINUS\n"+
"(select p.auction_id,p.name,p.description,p.amount from product p,bidlog bl where p.auction_id=bl.auction_id and bl.bidder=?)) natural join belongsto"; 
			PreparedStatement prep = connection.prepareStatement(query);
			prep.setString(1, username);
			prep.setString(2, username);
			prep.setString(3, username);
			ResultSet rs = prep.executeQuery();
			System.out.println("===========================SUGGESTIONS==========================");
			String fmtstring = "%11s %15s %25s %15s %15s\n";
			System.out.printf(fmtstring, "Auction ID", "Name", "Description", "Category", "Highest Bid");
			while(rs.next()) {
				System.out.printf(fmtstring, ""+rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), ""+rs.getInt(4));
			}
			rs.close();
			System.out.println("================================================================");
		 }
		 catch(Exception e) {
			 System.out.println("Error: " + e);
		 }
		 /*
		  * 
select * from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id)
where prodz.status='underauction' and logz.bidder in 
(select bidder from bidlog log2 
where log2.bidder<>'TheUserID' and log2.auction_id in 
(select prod.auction_id as auction_id from (product prod join bidlog loga on loga.auction_id=prod.auction_id)
where loga.bidder='TheUserID' and prod.status='underauction'));
		  */
		 
/*		 HashSet<Integer> myauctions = new HashSet<Integer>();
		 HashMap<String, HashSet<Integer>> friends = new HashMap<String, HashSet<Integer>>();
		 HashSet<Integer> suggestions = new HashSet<Integer>();
		 
		 try {
			 //ResultSet rs = mm.doQuery("select prodz.auction_id from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id) where prodz.status='underauction' and logz.bidder='"+username+"'");
			 ResultSet rs = mm.doQuery("select prodz.auction_id from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id) where logz.bidder='"+username+"'");
			 while(rs.next()) {
				 myauctions.add(rs.getInt(1));
			 }
			 rs.close();
			 
			 //rs = mm.doQuery("select logz.bidder,prodz.auction_id from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id) where prodz.status='underauction' and logz.bidder<>'"+username+"'");
			 rs = mm.doQuery("select logz.bidder,prodz.auction_id from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id) where logz.bidder<>'"+username+"'");
			 while(rs.next()) {
				 if(friends.get(rs.getString(1)) == null)
					 friends.put(rs.getString(1), new HashSet<Integer>());
				 friends.get(rs.getString(1)).add(rs.getInt(2));
			 }
			 rs.close();
			 
			 for(String friend : friends.keySet()) {
				 HashSet<Integer> cur = friends.get(friend);
				 // it matches!
				 if(cur.containsAll(myauctions)) {
					 cur.removeAll(myauctions);
					 suggestions.addAll(cur);
				 }
			 }
			 
			String query = "select p.auction_id,p.name,description,category,amount from (product p join belongsto bt on p.auction_id=bt.auction_id) where p.auction_id=? and p.status='underauction'";
	
			System.out.println("===========================SUGGESTIONS==========================");
			String fmtstring = "%11s %15s %25s %15s %15s\n";
			System.out.printf(fmtstring, "Auction ID", "Name", "Description", "Category", "Highest Bid");
			for(Integer i : suggestions) {
				PreparedStatement prep = connection.prepareStatement(query);
				prep.setInt(1, i);
				rs = prep.executeQuery();
				if(rs.next()) {
					System.out.printf(fmtstring, ""+rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), ""+rs.getInt(5));
				}
			}
			System.out.println("================================================================");
			 
		 }
		 catch(Exception e) {
			 System.out.println("Error: " + e);
		 }*/
	 }
	 
	 private boolean isInNestedSet(String cat, HashMap<String,HashSet<String>> categories) {
		 if(categories.containsKey(cat)) {
			 return true;
		 }
		 
		 for(String k : categories.keySet()) {
			 for(String val : categories.get(k)) {
				 if(val.equals(cat))
					 return true;
			 }
		 }
		 
		 return false;
	 }
	 
	 private String getParentCategory(String cat, HashMap<String,HashSet<String>> categories) {
		 if(!isInNestedSet(cat, categories)) {
			 return "";
		 }
		 
		 for(String k : categories.keySet()) {
			 for(String val : categories.get(k)) {
				 if(val.equals(cat))
					 return k;
			 }
		 }
		 
		 return "";
	 }
	 
	 public void doPutProduct(Scanner sc) {
		 String auction_title = "";
		 String description = "";
		 String category = "";
		 int numdays = 0;
		 
		 System.out.print("Enter a title for your auction: ");
		 auction_title = sc.nextLine();
		 System.out.print("Enter a description (hit enter if none): ");
		 description = sc.nextLine();
		 
		 // Get categories
		 HashMap<String,HashSet<String>> categories = new HashMap<String,HashSet<String>>();
		 // put parent categories in hashmap
		 try {
			 ResultSet rs = mm.doQuery("select name from category where parent_category is null");
			 while(rs.next()) {
				 categories.put(rs.getString(1), new HashSet<String>());
			 }
			 rs = mm.doQuery("select name,parent_category from category where parent_category is not null");
			 while(rs.next()) {
				 if(!categories.containsKey(rs.getString(2)))
					 categories.put(rs.getString(2), new HashSet<String>());
				 categories.get(rs.getString(2)).add(rs.getString(1));
			 }
		} catch (Exception e) {
			System.out.println("SQL Error: " + e);
		}
		System.out.println("Which category would you like to list under?");
		for(String k : categories.keySet()) {
			System.out.println("\t"+k);
			for(String cat : categories.get(k)) {
				System.out.println("\t\t"+cat);
			}
		}

		do {
			System.out.print("Enter your choice: ");
			category = sc.nextLine();
			
			if(!isInNestedSet(category, categories)) {
				System.out.println("Error: that is not a valid category");
			}
		}
		while(!isInNestedSet(category, categories));
		 System.out.print("Enter the number of days you are listing this item for: ");
		 numdays = sc.nextInt(); sc.nextLine();
		 
		 String execquery = "{CALL put_product(?,?,?,?,?,?)}";
		 // create or replace procedure put_product (seller IN varchar2, name IN varchar2, description IN varchar2, category_p IN varchar2, numdays IN integer, auctionid OUT integer)
		 try {
			CallableStatement caller = connection.prepareCall(execquery);
			caller.setString(1, username);
			caller.setString(2, auction_title);
			caller.setString(3, description);
			caller.setString(4, category);
			caller.setInt(5, numdays);
			caller.registerOutParameter(6, java.sql.Types.INTEGER);
			caller.execute();
			
			int new_auctionid = caller.getInt(6);
			
			if(new_auctionid != -1) {
				PreparedStatement prep = null;
				
				// insert into parent as well
				String parentcat = getParentCategory(category, categories);
				if(!parentcat.equals("")) {
					prep = connection.prepareStatement("insert into belongsto (auction_id, category) values (?, ?)");
					prep.setInt(1, new_auctionid);
					prep.setString(2, parentcat);
					prep.executeUpdate();
	
					parentcat = getParentCategory(parentcat, categories);
					while(!parentcat.equals("")) {
						prep = connection.prepareStatement("insert into belongsto (auction_id, category) values (?, ?)");
						prep.setInt(1, new_auctionid);
						prep.setString(2, parentcat);
						prep.executeUpdate();
						parentcat = getParentCategory(parentcat, categories);
					}
				}
			}
			else {
				System.out.println("Error: auction insertion failed.");
				return;
			}
			connection.commit();
			
			System.out.println("Your auction has been successfully listed! Auction ID #: " + caller.getInt(6));
			
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e);
		}
	 }

	 void doSearchProducts(Scanner sc) {
		 String searchquery = "select p.auction_id,p.name,description,bt.category,amount from (product p join belongsto bt on p.auction_id=bt.auction_id) where upper(description) like ?";

		 String keyword1 = "";
		 String keyword2 = "";
		 
		 try {
			System.out.print("Enter a keyword to search: ");
			keyword1 = sc.nextLine();
			System.out.print("Enter another keyword to search (press enter for none): ");
			keyword2 = sc.nextLine();
			if(keyword2.length() > 0) {
				searchquery += " and description like ?";
			}
			PreparedStatement prep = connection.prepareStatement(searchquery);
			prep.setString(1, "%"+keyword1.toUpperCase()+"%");
			if(keyword2.length() > 0) {
				prep.setString(2, "%"+keyword2.toUpperCase()+"%");
			}
			ResultSet rs = prep.executeQuery();
			System.out.println("=============================RESULTS============================");
			String fmtstring = "%11s %15s %25s %15s %15s\n";
			System.out.printf(fmtstring, "Auction ID", "Name", "Description", "Category", "Highest Bid");
			while(rs.next()) {
				System.out.printf(fmtstring, rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), ""+rs.getInt(5));
			}
			System.out.println("================================================================");
			rs.close();
			
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e);
		}
	 }
	 
	 void doBrowseProducts(Scanner sc) {
		 String category = "Books";
		 try {
			 // Get categories
			 HashMap<String,HashSet<String>> categories = new HashMap<String,HashSet<String>>();
			 // put parent categories in hashmap
			 try {
				 ResultSet rs = mm.doQuery("select name from category where parent_category is null");
				 while(rs.next()) {
					 categories.put(rs.getString(1), new HashSet<String>());
				 }
				 rs = mm.doQuery("select name,parent_category from category where parent_category is not null");
				 while(rs.next()) {
					 if(!categories.containsKey(rs.getString(2)))
						 categories.put(rs.getString(2), new HashSet<String>());
					 categories.get(rs.getString(2)).add(rs.getString(1));
				 }
			} catch (Exception e) {
				System.out.println("SQL Error: " + e);
			}
			System.out.println("Which category would you like to list under?");
			for(String k : categories.keySet()) {
				System.out.println("\t"+k);
				for(String cat : categories.get(k)) {
					System.out.println("\t\t"+cat);
				}
			}

			do {
				System.out.print("Enter your choice: ");
				category = sc.nextLine();
				
				if(!isInNestedSet(category, categories)) {
					System.out.println("Error: that is not a valid category");
				}
			}
			while(!isInNestedSet(category, categories));
			String browsequery = "select p.auction_id,p.name,description,amount from (product p join belongsto bt on p.auction_id=bt.auction_id) where bt.category=? and p.status='underauction'";
			
			int sort = 0;
			do {
				System.out.println("What would you like to sort by?");
				System.out.println("\t1: Highest bid amount");
				System.out.println("\t2: Alphabetically by product name");
				System.out.print("Enter your choice: ");
				sort = sc.nextInt(); sc.nextLine();
				switch(sort) {
				case 1:
					browsequery += " order by amount desc";
					break;
				case 2:
					browsequery += " order by p.name asc";
					break;
				default:
				}
			}
			while(sort != 1 && sort != 2);
			
			System.out.println("=============================RESULTS============================");
			String fmtstring = "%11s %15s %25s %15s\n";
			System.out.printf(fmtstring, "Auction ID", "Name", "Description", "Highest Bid");
			if(getParentCategory(category, categories).equals("")) { // browsing a parent category
				// get the children nodes
				for(String cat : categories.get(category)) {
					PreparedStatement prep = connection.prepareStatement(browsequery);
					prep.setString(1, cat);
					ResultSet rs = prep.executeQuery();
					while(rs.next()) {
						// auction id, name, description (if search), highest bid amount (if order by highest bid).
						System.out.printf(fmtstring, rs.getString(1), rs.getString(2), rs.getString(3), ""+rs.getInt(4));
					}
					rs.close();
				}
			}
			//else {
				PreparedStatement prep = connection.prepareStatement(browsequery);
				prep.setString(1, category);
				ResultSet rs = prep.executeQuery();
				while(rs.next()) {
					// auction id, name, description (if search), highest bid amount (if order by highest bid).
					System.out.printf(fmtstring, rs.getString(1), rs.getString(2), rs.getString(3), ""+rs.getInt(4));
				}
				rs.close();
			//}
			System.out.println("================================================================");
			
		} catch (SQLException e) {
			System.out.println("SQL Error: " + e);
		}
	 }


	 //Method to allow the seller to either sell or withdraw their product after looking at the SECOND highest bid
	 public void sellProduct(Scanner sc)
	 {
	 	int auction_id;
	 	int bid_id = -1;
	 	
	 	System.out.print("Enter the Auction ID of the item you wish to sell/withdraw: ");
	 	
	 	auction_id = sc.nextInt();
	 	sc.nextLine();

	 	String query1 = "create table Temp as select * from BIDLOG where BIDLOG.Auction_id = " + auction_id + " order by BIDLOG.Amount DESC"; 

	 	String query2 = "select * from Temp where rownum <= 2";

	 	String query3 = "drop table Temp";
	 	String query4 = "purge recyclebin";

	 	try
	 	{
	 		//Create temporary table
	 		mm.doQuery(query1);
	 		connection.commit();

	 		//Set the result set to be scrollable so we can reach last row
	 		Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	 		//Select rows from table
	 		ResultSet results = stmt.executeQuery(query2);

	 		System.out.println("Bidsn\tAuction_id\tBidder\tBid Time\t\tAmount");
			System.out.println("-------------------------------------------------------------");

			while (results.next())
			{
				results.last(); //Move to last row
				bid_id = results.getInt(1);

			 	System.out.printf("%d\t%d\t%s\t%s\t\t%d\n", bid_id, results.getInt(2), results.getString(3), results.getDate(4).toString(), results.getInt(5));
			}

			//Drop temporary table
			mm.doQuery(query3);
			connection.commit();

			//Purge recyclebin
			mm.doQuery(query4);
			connection.commit();

	 	}
	 	catch(Exception se) {
			System.out.println("Error: Failed to retrieve 2nd highest bid.");
			System.out.println(se.getMessage());
	 	}

	 	//Figure out what seller wants to do now that they've seen the second highest bid
	 	System.out.print("Enter 1 to sell, 2 to withdraw, 0 to exit: ");

	 	int choice = sc.nextInt();
	 	sc.nextLine();
	 	
	 	boolean withdraw = false;
	 	boolean sell = false;

	 	switch (choice)
	 	{
	 		case 0:
	 			break;
	 		case 1:		//Sell product
	 			sell = true;
	 			break;
	 		case 2:		//Withdraw product
	 			withdraw = true;
	 			break;
	 		default:
	 			break;
	 	}


	 	if (sell)
	 	{
	 		try
	 		{
	 			//Sell product by calling the sell product function
	 			CallableStatement cas = connection.prepareCall("{CALL Sell_Product(?)}");
	 			cas.setInt(1, bid_id);
	 			cas.execute();

	 			connection.commit();
	 		}
	 		catch(Exception se) {
				System.out.println("Error: Failed to sell product.");
				System.out.println(se.getMessage());
	 		}

	 	}
	 	else if (withdraw)
	 	{
	 		try
	 		{
	 			//Withdraw product
	 			CallableStatement cas = connection.prepareCall("{CALL BJI6.Withdraw_Product(?)}");
	 			cas.setInt(1, auction_id);
	 			cas.execute();
	 			connection.commit();
	 		}
	 		catch(Exception se) {
				System.out.println("Error: Failed to withdraw product.");
				System.out.println(se.getMessage());
	 		}
	 	}

	 }
}
