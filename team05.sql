--Names: Ben Isenberg, Raymond Wang (Gengyu) 4/19/2013
--Usernames: BJI6, GEW11

--Drop all preexisting tables
drop table category cascade constraints;
drop table product cascade constraints;
drop table customer cascade constraints;
drop table administrator cascade constraints;
drop table ourdate cascade constraints;

drop table bidlog cascade constraints;

drop table belongsto cascade constraints;


--CREATE TABLES SECTION

--Empty recycle bin
PURGE RECYCLEBIN;

--Table keeps track of time for auction system
create table OURDATE
(
    C_date date
);

create table CUSTOMER
(
    Login varchar2(10) not null,
    Password varchar2(10),
    Name varchar2(20),
    Address varchar2(30),
    Email varchar2(20),
    --Primary Key
    constraint Customer_PK PRIMARY KEY(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table ADMINISTRATOR
(
    Login varchar2(10) not null,
    Password varchar2(10),
    Name varchar2(20),
    Address varchar2(30),
    Email varchar2(20),
    --Primary Key
    constraint Administrator_PK PRIMARY KEY(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table PRODUCT
(
    Auction_id int not null,
    Name varchar2(20),
    Description varchar2(30),
    Seller varchar2(10),
    Start_date date,
    Min_price int,    
    Number_of_days int,
    Status varchar2(15) not null,
    Buyer varchar2(10),
    Sell_date date,
    Amount int,
    --Primary Key
    constraint Product_PK PRIMARY KEY(Auction_id) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Keys
    constraint Product_FK1 FOREIGN KEY(Seller) references CUSTOMER(Login) DEFERRABLE INITIALLY IMMEDIATE,
    constraint Product_FK2 FOREIGN KEY(Buyer) references CUSTOMER(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table BIDLOG
(
    Bidsn int not null,
    Auction_id int,
    Bidder varchar2(10),
    Bid_time date,
    Amount int,
    --Primary Key
    constraint Bidlog_PK PRIMARY KEY(Bidsn) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Keys
    constraint Bidlog_FK1 FOREIGN KEY(Auction_id) references PRODUCT(Auction_id) DEFERRABLE INITIALLY IMMEDIATE,
    constraint Bidlog_FK2 FOREIGN KEY(Bidder) references CUSTOMER(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table CATEGORY
(
    Name varchar2(20) not null,
    Parent_category varchar2(20),
    --Primary Key
    constraint Category_PK PRIMARY KEY(Name) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Key
    constraint Category_FK FOREIGN KEY(Parent_category) references CATEGORY(Name) DEFERRABLE INITIALLY IMMEDIATE
);

create table BELONGSTO
(
    Auction_id int not null,
    Category varchar2(20) not null,
    --Primary key
    constraint Belongsto_PK PRIMARY KEY(Auction_id, Category) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Key
    constraint Belongsto_FK1 FOREIGN KEY(Auction_id) references PRODUCT(Auction_id) DEFERRABLE INITIALLY IMMEDIATE,
    constraint Belongsto_FK2 FOREIGN KEY(Category) references CATEGORY(Name) DEFERRABLE INITIALLY IMMEDIATE
);



-- INSERT DATA SECTION

--Insert sample data
INSERT INTO OURDATE
VALUES ( TO_DATE('2012-12-11 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.') );

INSERT INTO ADMINISTRATOR
VALUES ('admin', 'root', 'adminstrator', '6810 SENSQ', 'admin@1555.com');

INSERT INTO CUSTOMER
VALUES ('user0', 'pwd', 'user0', '6810 SENSQ', 'user0@1555.com');

INSERT INTO CUSTOMER
VALUES ('user1', 'pwd', 'user1', '6811 SENSQ', 'user1@1555.com');

INSERT INTO CUSTOMER
VALUES ('user2', 'pwd', 'user2', '6812 SENSQ', 'user2@1555.com');

INSERT INTO CUSTOMER
VALUES ('user3', 'pwd', 'user3', '6813 SENSQ', 'user3@1555.com');

INSERT INTO CUSTOMER
VALUES ('user4', 'pwd', 'user4', '6814 SENSQ', 'user4@1555.com');

--I had to use Oracle's TO_DATE function to insert time information into a DATE type
INSERT INTO PRODUCT
VALUES (0, 'Database', 'SQL ER-design', 'user0', TO_DATE('2012-12-04 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.'), 50, 2, 'sold', 'user2', TO_DATE('2012-12-06 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 53);

INSERT INTO PRODUCT
VALUES (1, '17 inch monitor', '17 inch monitor', 'user0', TO_DATE('2012-12-06 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 100, 2, 'sold', 'user4', TO_DATE('2012-12-08 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 110);

INSERT INTO PRODUCT (Auction_id, Name, Description, Seller, Start_date, Min_price, Number_of_days, Status)
VALUES (2, 'DELL INSPIRON 1100', 'DELL INSPIRON notebook', 'user0', TO_DATE('2012-12-07 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 500, 7, 'underauction');

INSERT INTO PRODUCT
VALUES (3, 'Return of the King', 'fantasy', 'user1', TO_DATE('2012-12-07 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40, 2, 'sold', 'user2', TO_DATE('2012-12-09 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO PRODUCT
VALUES (4, 'The Sorcerer Stone', 'Harry Potter series', 'user1', TO_DATE('2012-12-08 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40, 2, 'sold', 'user3', TO_DATE('2012-12-10 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO PRODUCT (Auction_id, Name, Description, Seller, Start_date, Min_price, Number_of_days, Status)
VALUES (5, 'DELL INSPIRON 1100', 'DELL INSPIRON notebook', 'user1', TO_DATE('2012-12-09 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 200, 1, 'withdrawn');

INSERT INTO PRODUCT (Auction_id, Name, Description, Seller, Start_date, Min_price, Number_of_days, Status, Amount)
VALUES (6, 'Advanced Database', 'SQL Transaction index', 'user1', TO_DATE('2012-12-10 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 50, 2, 'underauction', 55);

INSERT INTO BIDLOG
VALUES (0, 0, 'user2', TO_DATE('2012-12-04 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 50);

INSERT INTO BIDLOG
VALUES (1, 0, 'user3', TO_DATE('2012-12-04 09:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 53);

INSERT INTO BIDLOG
VALUES (2, 0, 'user2', TO_DATE('2012-12-05 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 60);

INSERT INTO BIDLOG
VALUES (3, 1, 'user4', TO_DATE('2012-12-06 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 100);

INSERT INTO BIDLOG
VALUES (4, 1, 'user2', TO_DATE('2012-12-07 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 110);

INSERT INTO BIDLOG
VALUES (5, 1, 'user4', TO_DATE('2012-12-07 09:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 120);

INSERT INTO BIDLOG
VALUES (6, 3, 'user2', TO_DATE('2012-12-07 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO BIDLOG
VALUES (7, 4, 'user3', TO_DATE('2012-12-09 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO BIDLOG
VALUES (8, 6, 'user2', TO_DATE('2012-12-07 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 55);

INSERT INTO CATEGORY (Name)
VALUES ('Books');

INSERT INTO CATEGORY
VALUES ('Textbooks', 'Books');

INSERT INTO CATEGORY
VALUES ('Fiction books', 'Books');

INSERT INTO CATEGORY
VALUES ('Magazines', 'Books');

INSERT INTO CATEGORY
VALUES ('Computer Science', 'Textbooks');

INSERT INTO CATEGORY
VALUES ('Math', 'Textbooks');

INSERT INTO CATEGORY
VALUES ('Philosophy', 'Textbooks');

INSERT INTO CATEGORY (Name)
VALUES ('Computer Related');

INSERT INTO CATEGORY
VALUES ('Desktop PCs', 'Computer Related');

INSERT INTO CATEGORY
VALUES ('Laptops', 'Computer Related');

INSERT INTO CATEGORY
VALUES ('Monitors', 'Computer Related');

INSERT INTO CATEGORY
VALUES ('Computer books', 'Computer Related');

INSERT INTO BELONGSTO
VALUES (0, 'Computer Science');

INSERT INTO BELONGSTO
VALUES (0, 'Computer books');

INSERT INTO BELONGSTO
VALUES (1, 'Monitors');

INSERT INTO BELONGSTO
VALUES (2, 'Laptops');

INSERT INTO BELONGSTO
VALUES (3, 'Fiction books');

INSERT INTO BELONGSTO
VALUES (4, 'Fiction books');

INSERT INTO BELONGSTO
VALUES (5, 'Laptops');

INSERT INTO BELONGSTO
VALUES (6, 'Computer Science');

INSERT INTO BELONGSTO
VALUES (6, 'Computer books');

-- ADD BEN'S SQL CODE

--Part 2D
--Trigger to increment system time by 5 sec whenever a bid is inserted
create or replace trigger tri_bidTimeUpdate
after insert on BIDLOG
for each row
begin
UPDATE OURDATE
set OURDATE.C_date = OURDATE.C_date + (5/24/60/60); 
end;
/

--Part 2D
--Trigger to update PRODUCT table with new highest bid if a higher bid is inserted into BIDLOG
create or replace trigger tri_updateHighBid
after insert on BIDLOG
for each row
begin
UPDATE PRODUCT
set PRODUCT.Amount = case 
					 when ((PRODUCT.Amount is null or :new.Amount > PRODUCT.Amount) and :new.Amount >= PRODUCT.Min_price) then :new.Amount
					 else PRODUCT.Amount
					 end
where PRODUCT.Auction_id = :new.Auction_id;
end;
/

--Bid on product procedure
create or replace procedure Bid_On_Product(amount in int, auc_id in int, bidder in varchar2)
as
	bidder2 varchar2(10);
	status varchar2(15);
	bid_id int;
	b_date date;
begin
	bidder2 := bidder; --Need to place bidder name into a new varchar variable due to argument passing issues with varchar type
	--Get info needed for insert into BIDLOG table
	select count(*) into bid_id 
	from BIDLOG;
	select OURDATE.C_date into b_date 
	from OURDATE;
	select PRODUCT.Status into status
	from PRODUCT
	where PRODUCT.Auction_id = auc_id;

	if status = 'underauction' then
		insert into BIDLOG
		values (bid_id, auc_id, bidder2, b_date, amount);
	end if;

end;
/

--2E Seller functions

--2E Displaying second highest bid commands (an int from the user will be placed for Auction ID)
--Once the user has this information they can call the sell product or withdraw product function with correct bidlog id
create table Temp as
	select * 
	from BIDLOG
	where BIDLOG.Auction_id = 0
	order by BIDLOG.Amount DESC;

select * 
from Temp
where rownum <= 2;

drop table Temp;
purge recyclebin;



--Version 1 - sells specified product (parameter is the bid id)
--this procedure does DML queries
create or replace procedure Sell_Updates(auc_id in int, my_date in date, bid_value in int, buyer in varchar2)
as
	bb varchar2(10);
begin
	bb := buyer;
	--Update product table to reflect sale
	UPDATE PRODUCT
	set PRODUCT.Sell_date = my_date
	where PRODUCT.Auction_id = auc_id;

	UPDATE PRODUCT
	set PRODUCT.Amount = bid_value
	where PRODUCT.Auction_id = auc_id;

	UPDATE PRODUCT
	set PRODUCT.Buyer = bb
	where PRODUCT.Auction_id = auc_id;

	UPDATE PRODUCT
	set PRODUCT.Status = 'sold'
	where PRODUCT.Auction_id = auc_id;
end;
/

create or replace procedure Sell_Product (bid_id in int)
as
	auc_id int;
	my_date date;
	bid_value int;
	buyer varchar2(10);
begin
	--get current date time
	select OURDATE.C_date into my_date 
	from OURDATE;
	--get auction id of bid
	select BIDLOG.Auction_id into auc_id
	from BIDLOG
	where BIDLOG.Bidsn = bid_id;
	--get bid amount
	select BIDLOG.Amount into bid_value
	from BIDLOG
	where BIDLOG.Bidsn = bid_id;
	--get buyer id
	select BIDLOG.Bidder into buyer
	from BIDLOG
	where BIDLOG.Bidsn = bid_id;
	
	Sell_Updates(auc_id, my_date, bid_value, buyer);

end;
/

--Version 2 - withdraws specified product (parameter is the auction id)
--returns SUCCESS
create or replace procedure Withdraw_Product (auc_id in int)
as
	temp int;
begin
	UPDATE PRODUCT
	set PRODUCT.Status = 'withdrawn'
	where PRODUCT.Auction_id = auc_id;
end;
/


--Part 3B
--Statement to update System time, a string from user will be placed in to_date function
UPDATE OURDATE
set OURDATE.C_date =  TO_DATE('2012-12-11 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.');

--3C
--SQl code to display product statistics for all sellers
select PRODUCT.Name, PRODUCT.Status, PRODUCT.Amount, BIDLOG.Bidder
from PRODUCT, BIDLOG
where PRODUCT.Auction_id = BIDLOG.Auction_id and PRODUCT.Amount = BIDLOG.Amount;

--Login name is a string for specific seller (set to user0)
select PRODUCT.Name, PRODUCT.Status, PRODUCT.Amount, BIDLOG.Bidder
from PRODUCT, BIDLOG
where PRODUCT.Auction_id = BIDLOG.Auction_id and PRODUCT.Amount = BIDLOG.Amount and PRODUCT.Seller = 'user0';


--Part 3D
--Function to count the number of products sold in past X months for category C
-- have to convert input into varchar2(20) using temporary variable otherwise comparisons don't work for some odd reason
create or replace function Product_Count ( category in varchar2, num_months in int ) 
	return int
is
	min_date date;
	total_count int;
	c varchar2(20);
begin
	select ADD_MONTHS(OURDATE.C_date, -num_months) into min_date 
	from dual, OURDATE;
	c := category;
	select Count(*) into total_count
	from (select Product.Auction_id from PRODUCT where PRODUCT.Status = 'sold' and PRODUCT.Sell_date >= min_date) Temp1, 
	(select * from BELONGSTO where BELONGSTO.Category = c) Temp2
	where Temp1.Auction_id = Temp2.Auction_id;
	return total_count;
end;
/


--Function to count the number of bids user U has placed in past X months
create or replace function Bid_Count (user in varchar2, num_months in int)
	return int
is
	min_date date;
	b_count int;
begin
	select ADD_MONTHS(OURDATE.C_date, -num_months) into min_date 
	from dual, OURDATE;
	select COUNT(*) into b_count
	from BIDLOG
	where BIDLOG.Bidder = user and BIDLOG.Bid_time >= min_date;
	return b_count;
end;
/

--Function to calculate total user U has spent in past X months
create or replace function Buying_Amount (user in varchar2, num_months in int)
	return int
is
	total_spent int;
	min_date date;
begin
	select ADD_MONTHS(OURDATE.C_date, -num_months) into min_date 
	from dual, OURDATE;
	select SUM(PRODUCT.Amount) into total_spent
	from PRODUCT
	where PRODUCT.Buyer = user and PRODUCT.Sell_date >= min_date and PRODUCT.Status = 'sold';
	return total_spent;
end;
/


--Top K sold products which are root categories (parent = NULL) (K = 1 and X = 5)
create table Temp as
select CATEGORY.Name, Product_Count(CATEGORY.Name, 5) as p_count
from CATEGORY
where CATEGORY.Parent_category IS NULL
order by p_count DESC;

select Temp.Name, Temp.p_count
from Temp
where rownum <= 1;

drop table Temp;
purge recyclebin;

--Top K sold products which are leaf categories (not parents to others) (K = 3 and X = 5)
create table Temp as
select C.Name, Product_Count(C.Name, 5) as p_count
from CATEGORY C
where not exists (select null from CATEGORY where C.Name = CATEGORY.Parent_category)
order by p_count DESC;

select Temp.Name, Temp.p_count
from Temp
where rownum <= 3;

drop table Temp;
purge recyclebin;

--Top K most active bidders in past X months (K = 1 and X = 3)
create table Temp as
select distinct BIDLOG.Bidder, Bid_Count(BIDLOG.Bidder, 3) as Bid_Amount
from BIDLOG
order by Bid_Amount DESC;

select Temp.Bidder
from Temp
where rownum <= 1;

drop table Temp;
purge recyclebin;

--Top K most active spenders in past X months (K = 2 and X = 3)
create table Temp as
select distinct BIDLOG.Bidder, Buying_Amount(BIDLOG.Bidder, 3) as Amount_spent
from BIDLOG
order by Amount_spent DESC;

select Temp.Bidder
from Temp
where rownum <= 2;

drop table Temp;
purge recyclebin;


--Part 4B
--Trigger to close all auctions in PRODUCT table that have a sell date before new system time inserted in OURDATE table
--calculate if the amount of time the product was set to be placed on auction is greater than system date 
create or replace trigger closeAuctions
after update on OURDATE
for each row
begin
UPDATE PRODUCT
set PRODUCT.Status = 'closed'
where (PRODUCT.Start_date + PRODUCT.Number_of_days) < :new.C_date and PRODUCT.status = 'underauction';
end;
/


--ADD RAY'S SQL CODE

-- 2a Browsing Products
select * from (product p join belongsto bt on p.auction_id=bt.auction_id) where bt.category='Books' and p.status='underauction';

-- 2b Search for product by text
select * from product where DESCRIPTION like '%keyword1%' and DESCRIPTION like '%keyword2%';

-- 2c Putting products for auction
create or replace procedure put_product (seller IN varchar2, name IN varchar2, description IN varchar2, category_p IN varchar2, numdays IN integer, auctionid OUT integer)
as
	startdate date;
	validcat number;
begin
	-- check Category(name, parent_category) for valid entry passed in "category" parameter
	select count(*) into validcat from category where name like '%' || category_p || '%';
	
	-- category check fail
	if validcat <= 0 then
		auctionid:= -1;
		return;
	end if;

	-- initialize
	auctionid := 0;
	startdate := TO_DATE('2012-01-01 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.');
	-- select the highest if possible
	select max(auction_id) into auctionid from product;
	-- increment it
	auctionid := auctionid + 1;
	-- get date from the OURDATE table (NEEDS OURDATE TABLE TO EXIST BEFORE THIS PROCEDURE WILL WORK)
	select C_date into startdate from OURDATE;
	
	-- need to insert into product and BelongsTo(auction_id, category)
	insert into product (auction_id, name, description, buyer, seller, start_date, number_of_days, status)
			values(auctionid, name, description, seller, seller, startdate, numdays, 'underauction');
	insert into belongsto (auction_id, category) values (auctionid, category_p);

	return;
end put_product;
/
show errors;


-- 2f Suggestions
select * from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id)
where logz.bidder in 
(select bidder from bidlog log2 
where log2.bidder<>'TheUserID' and log2.auction_id in 
(select prod.auction_id as auction_id from (product prod join bidlog loga on loga.auction_id=prod.auction_id)
where loga.bidder='TheUserID' and prod.status='underauction'));

-- 3a New customer registration
insert into CUSTOMER  values ('TheUser', 'Password', 'TheName', 'TheAddress', 'Email@WebSite.com');

-- 4a Write the SQL statement that will return the information for a user, given a login
name and password. If the login name + password does not match any entry in
the database the system returns an empty list.

select * from ADMINISTRATOR where LOGIN='TheUser' and PASSWORD='Password'
union
select * from CUSTOMER where LOGIN='TheUser' and PASSWORD='Password';