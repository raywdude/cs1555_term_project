--Names: Ben Isenberg, Raymond Wang (Gengyu) 4/4/2013
--Usernames: BJI6, GEW11

--Part 2D
--Trigger to increment system time by 5 sec whenever a bid is inserted
create or replace trigger tri_bidTimeUpdate
after insert on BIDLOG
for each row
begin
UPDATE OURDATE
set OURDATE.C_date = OURDATE.C_date + (5/24/60/60); 
end;
/

--Part 2D
--Trigger to update PRODUCT table with new highest bid if a higher bid is inserted into BIDLOG
create or replace trigger tri_updateHighBid
after insert on BIDLOG
for each row
begin
UPDATE PRODUCT
set PRODUCT.Amount = case 
					 when ((PRODUCT.Amount is null or :new.Amount > PRODUCT.Amount) and :new.Amount >= PRODUCT.Min_price) then :new.Amount
					 else PRODUCT.Amount
					 end
where PRODUCT.Auction_id = :new.Auction_id;
end;
/

--Bid on product procedure
create or replace procedure Bid_On_Product(amount in int, auc_id in int, bidder in varchar2)
as
	bidder2 varchar2(10);
	status varchar2(15);
	bid_id int;
	b_date date;
begin
	bidder2 := bidder; --Need to place bidder name into a new varchar variable due to argument passing issues with varchar type
	--Get info needed for insert into BIDLOG table
	select count(*) into bid_id 
	from BIDLOG;
	select OURDATE.C_date into b_date 
	from OURDATE;
	select PRODUCT.Status into status
	from PRODUCT
	where PRODUCT.Auction_id = auc_id;

	if status = 'underauction' then
		insert into BIDLOG
		values (bid_id, auc_id, bidder2, b_date, amount);
	end if;

end;
/

--2E Seller functions

--2E Displaying second highest bid commands (an int from the user will be placed for Auction ID)
--Once the user has this information they can call the sell product or withdraw product function with correct bidlog id
create table Temp as
	select * 
	from BIDLOG
	where BIDLOG.Auction_id = 0
	order by BIDLOG.Amount DESC;

select * 
from Temp
where rownum <= 2;

drop table Temp;
purge recyclebin;



--Version 1 - sells specified product (parameter is the bid id)
--this procedure does DML queries
create or replace procedure Sell_Updates(auc_id in int, my_date in date, bid_value in int, buyer in varchar2)
as
	bb varchar2(10);
begin
	bb := buyer;
	--Update product table to reflect sale
	UPDATE PRODUCT
	set PRODUCT.Sell_date = my_date
	where PRODUCT.Auction_id = auc_id;

	UPDATE PRODUCT
	set PRODUCT.Amount = bid_value
	where PRODUCT.Auction_id = auc_id;

	UPDATE PRODUCT
	set PRODUCT.Buyer = bb
	where PRODUCT.Auction_id = auc_id;

	UPDATE PRODUCT
	set PRODUCT.Status = 'sold'
	where PRODUCT.Auction_id = auc_id;
end;
/

create or replace procedure Sell_Product (bid_id in int)
as
	auc_id int;
	my_date date;
	bid_value int;
	buyer varchar2(10);
begin
	--get current date time
	select OURDATE.C_date into my_date 
	from OURDATE;
	--get auction id of bid
	select BIDLOG.Auction_id into auc_id
	from BIDLOG
	where BIDLOG.Bidsn = bid_id;
	--get bid amount
	select BIDLOG.Amount into bid_value
	from BIDLOG
	where BIDLOG.Bidsn = bid_id;
	--get buyer id
	select BIDLOG.Bidder into buyer
	from BIDLOG
	where BIDLOG.Bidsn = bid_id;
	
	Sell_Updates(auc_id, my_date, bid_value, buyer);

end;
/

--Version 2 - withdraws specified product (parameter is the auction id)
--returns SUCCESS
create or replace procedure Withdraw_Product (auc_id in int)
as
	temp int;
begin
	UPDATE PRODUCT
	set PRODUCT.Status = 'withdrawn'
	where PRODUCT.Auction_id = auc_id;
end;
/


--Part 3B
--Statement to update System time, a string from user will be placed in to_date function
UPDATE OURDATE
set OURDATE.C_date =  TO_DATE('2012-12-11 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.');

--3C
--SQl code to display product statistics for all sellers
select PRODUCT.Name, PRODUCT.Status, PRODUCT.Amount, BIDLOG.Bidder
from PRODUCT, BIDLOG
where PRODUCT.Auction_id = BIDLOG.Auction_id and PRODUCT.Amount = BIDLOG.Amount;

--Login name is a string for specific seller (set to user0)
select PRODUCT.Name, PRODUCT.Status, PRODUCT.Amount, BIDLOG.Bidder
from PRODUCT, BIDLOG
where PRODUCT.Auction_id = BIDLOG.Auction_id and PRODUCT.Amount = BIDLOG.Amount and PRODUCT.Seller = 'user0';


--Part 3D
--Function to count the number of products sold in past X months for category C
-- have to convert input into varchar2(20) using temporary variable otherwise comparisons don't work for some odd reason
create or replace function Product_Count ( category in varchar2, num_months in int ) 
	return int
is
	min_date date;
	total_count int;
	c varchar2(20);
begin
	select ADD_MONTHS(OURDATE.C_date, -num_months) into min_date 
	from dual, OURDATE;
	c := category;
	select Count(*) into total_count
	from (select Product.Auction_id from PRODUCT where PRODUCT.Status = 'sold' and PRODUCT.Sell_date >= min_date) Temp1, 
	(select * from BELONGSTO where BELONGSTO.Category = c) Temp2
	where Temp1.Auction_id = Temp2.Auction_id;
	return total_count;
end;
/


--Function to count the number of bids user U has placed in past X months
create or replace function Bid_Count (user in varchar2, num_months in int)
	return int
is
	min_date date;
	b_count int;
begin
	select ADD_MONTHS(OURDATE.C_date, -num_months) into min_date 
	from dual, OURDATE;
	select COUNT(*) into b_count
	from BIDLOG
	where BIDLOG.Bidder = user and BIDLOG.Bid_time >= min_date;
	return b_count;
end;
/

--Function to calculate total user U has spent in past X months
create or replace function Buying_Amount (user in varchar2, num_months in int)
	return int
is
	total_spent int;
	min_date date;
begin
	select ADD_MONTHS(OURDATE.C_date, -num_months) into min_date 
	from dual, OURDATE;
	select SUM(PRODUCT.Amount) into total_spent
	from PRODUCT
	where PRODUCT.Buyer = user and PRODUCT.Sell_date >= min_date and PRODUCT.Status = 'sold';
	return total_spent;
end;
/


--Top K sold products which are root categories (parent = NULL) (K = 1 and X = 5)
create table Temp as
select CATEGORY.Name, Product_Count(CATEGORY.Name, 5) as p_count
from CATEGORY
where CATEGORY.Parent_category IS NULL
order by p_count DESC;

select Temp.Name, Temp.p_count
from Temp
where rownum <= 1;

drop table Temp;
purge recyclebin;

--Top K sold products which are leaf categories (not parents to others) (K = 3 and X = 5)
create table Temp as
select C.Name, Product_Count(C.Name, 5) as p_count
from CATEGORY C
where not exists (select null from CATEGORY where C.Name = CATEGORY.Parent_category)
order by p_count DESC;

select Temp.Name, Temp.p_count
from Temp
where rownum <= 3;

drop table Temp;
purge recyclebin;

--Top K most active bidders in past X months (K = 1 and X = 3)
create table Temp as
select distinct BIDLOG.Bidder, Bid_Count(BIDLOG.Bidder, 3) as Bid_Amount
from BIDLOG
order by Bid_Amount DESC;

select Temp.Bidder
from Temp
where rownum <= 1;

drop table Temp;
purge recyclebin;

--Top K most active spenders in past X months (K = 2 and X = 3)
create table Temp as
select distinct BIDLOG.Bidder, Buying_Amount(BIDLOG.Bidder, 3) as Amount_spent
from BIDLOG
order by Amount_spent DESC;

select Temp.Bidder
from Temp
where rownum <= 2;

drop table Temp;
purge recyclebin;


--Part 4B
--Trigger to close all auctions in PRODUCT table that have a sell date before new system time inserted in OURDATE table
--calculate if the amount of time the product was set to be placed on auction is greater than system date 
create or replace trigger closeAuctions
after update on OURDATE
for each row
begin
UPDATE PRODUCT
set PRODUCT.Status = 'closed'
where (PRODUCT.Start_date + PRODUCT.Number_of_days) < :new.C_date and PRODUCT.status = 'underauction';
end;
/
