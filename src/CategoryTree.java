import java.util.*;

public class CategoryTree {
	public HashMap<String, CategoryTree> child;
	
	public CategoryTree() {
		child = new HashMap<String, CategoryTree>();
	}
	
	public boolean isInTree(String k) {
		if(child.containsKey(k)) 
			return true;
		for(String key : child.keySet()) {
			if(child.get(key) != null && child.get(key).isInTree(k)) {
				return true;
			}
		}
		return false;
	}
	
	public void insert_key(String key, String parent) {
		if(parent == null) {
			child.put(key, null);
			return;
		}
		
		if(child.containsKey(parent)) {
			if(child.get(parent) == null) {
				CategoryTree cat = new CategoryTree();
				cat.child.put(key, null);
				child.put(parent, cat);
				return;
			}
		}
		
		insert_key(this, key, parent);
	}
	
	private void insert_key(CategoryTree tree, String key, String parent) {
		if(tree.child.containsKey(parent)) {
			CategoryTree cat = new CategoryTree();
			cat.child.put(key, null);
			tree.child.put(parent, cat);
			return;
		}
		
		for(String k : tree.child.keySet()) {
			if(tree.child.get(k) != null) {
				insert_key(tree.child.get(k), key, parent);
			}
		}
	}
	
	private String multiplyChar(int num, String thechar) {
		String str = "";
		for(int i = 0; i < num; ++i) {
			str += thechar;
		}
		return str;
	}
	
	private String toString(CategoryTree tree, int level) {
		if(tree == null) {
			return "";
		}
		String str = "";
		for(String k : tree.child.keySet()) {
			str += "\t" + multiplyChar(level, "\t") + k + "\n";
			str += toString(tree.child.get(k),level+1);
		}
		return str;
	}
	
	public String toString() {
		return toString(this, 0);
	}
}
