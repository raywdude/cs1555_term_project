/**
 * CS1555 Term Project - myAuction (Group 5)
 * by Raymond Wang (Gengyu), Ben Isenberg
 * gew11@pitt.edu, bji6@pitt.edu
 * 
 * Copyright (C) 2013, All rights reserved.
 * 
 *	Redistribution and use in source and binary forms, with or without
 *	modification, are permitted provided that the following conditions are met:
 *	    * Redistributions of source code must retain the above copyright
 *	      notice, this list of conditions and the following disclaimer.
 *	    * Redistributions in binary form must reproduce the above copyright
 *	      notice, this list of conditions and the following disclaimer in the
 *	      documentation and/or other materials provided with the distribution.
 *	    * Neither the name of the <organization> nor the
 *	      names of its contributors may be used to endorse or promote products
 *	      derived from this software without specific prior written permission.
 *	
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *	DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;  //import the file containing definitions for the parts
                    import java.util.HashMap;
import java.util.HashSet;
//needed by java for database connection and manipulation
import java.util.Scanner;

public class MainMenu {
	 ////////////////// ENTER DATABASE ACCESS CREDENTIALS HERE //////////////////////////////////
	 private static final String DATABASE_USERNAME = "bji6";
	 private static final String DATABASE_PASSWORD = "3573308";
	 
     //TODO: COMMENT OUT THE OTHER WHEN CONNECTING ON/OFF CAMPUS
     //private static final String SERVER_URL = "jdbc:oracle:thin:@localhost:1521:dbclass"; // use off-campus via SSH tunnel
     private static final String SERVER_URL = "jdbc:oracle:thin:@db10.cs.pitt.edu:1521:dbclass"; // use on-campus
	 ////////////////////////////////////////////////////////////////////////////////////////////
	 
	 private Connection connection; //used to hold the jdbc connection to the DB
	 private String username, password;
	 
	 private boolean bIsAdmin;
	 private boolean bIsLoggedIn;
	 private boolean bIsConnected;
	 
	 private CustomerMenu cm;
	 private AdminMenu am;
	 
	 public MainMenu(String u, String p) {
		 bIsAdmin = false;
		 bIsConnected = false;
		 bIsLoggedIn = false;
		 username = u;
		 password = p;
		 cm = null;
		 am = null;
	 }
	 
	 public boolean doDBConnect() {
		    try{
		        DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
		        
		        connection = DriverManager.getConnection(SERVER_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
		        if(connection == null)
		        	return false;
		        connection.setAutoCommit(false);
		        //create a connection to DB on db10.cs.pitt.edu
		        bIsConnected = true;
		        return true;
		      }
		      catch(Exception Ex)  //What to do with any exceptions
		      {
		        System.out.println("Error connecting to database.  Machine Error: " +
		              Ex.toString());
		  	Ex.printStackTrace();
		  	return false;
		      }

	 }
	 
	 public void doDBDisconnect() {
		 if(bIsConnected) {
			 bIsConnected = false;
			 bIsLoggedIn = false;
			 try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 }
	 }
	 
	 /**
	  * Executes queries directly and also allows executing queries stored in files
	  * Note: Do not use for user-input queries; use PreparedStatement instead.
	  * by Raymond Wang
	  * 
	  * @param queryStringOrFilePath
	  * @param bIsFilePath
	  * @return resultset, if the query returns anything
	  */
	public ResultSet doQuery(String queryStringOrFilePath, boolean bIsFilePath) {
		ResultSet rs = null;
	      try {
	    	  if(bIsFilePath) {
	    		  FileInputStream fi = new FileInputStream(queryStringOrFilePath);
	    		  Scanner sc = new Scanner(fi);
	    		  StringBuffer sb = new StringBuffer();
	    		  sb.append(sc.nextLine());
	    		  sc.close();
	    		  fi.close();
	    		  // put what I read from file into the query
	    		  queryStringOrFilePath = sb.toString();
	    	  }
	    	Statement statement = connection.createStatement();
		    rs = statement.executeQuery(queryStringOrFilePath);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	/**
	 * The simple version of the above function, except this one only executes query strings, no files
	 * @param queryString
	 * @return
	 */
	public ResultSet doQuery(String queryString) {
		return doQuery(queryString, false);
	}
	 
	 public boolean doLogin() {
		 // Need to connect to DBMS
		 if(!bIsConnected) {
			 System.out.println("Error: Not connected to database system. Please connect to DBMS first.");
			 return false;
		 }
		 
		 // Not safe from SQL Injections
		 /*String adminquery = "select * from ADMINISTRATOR where LOGIN='"+username+"' and PASSWORD='"+password+"'";
		 String customerquery = "select * from CUSTOMER where LOGIN='"+username+"' and PASSWORD='"+password+"'";

		 ResultSet rs1 = doQuery(adminquery);
		 ResultSet rs2 = doQuery(customerquery);*/
		 
		 String adminquery = "select * from ADMINISTRATOR where LOGIN=? and PASSWORD=?";
		 String customerquery = "select * from CUSTOMER where LOGIN=? and PASSWORD=?";
		 PreparedStatement prep1 = null;
		 PreparedStatement prep2 = null;
		 try {
			prep1 = connection.prepareStatement(adminquery);
			prep2 = connection.prepareStatement(customerquery);
			prep1.setString(1, username); prep1.setString(2, password);
			prep2.setString(1, username); prep2.setString(2, password);
			ResultSet rs1 =  prep1.executeQuery();
			ResultSet rs2 = prep2.executeQuery();
			if(rs1 != null && rs2 != null) {
				 // check if admin, if so, set bIsAdmin
				if(rs1.next()) {
					bIsAdmin = true;
				 }
				else if(rs2.next()) {
					bIsAdmin = false;
				}
				else {
					return false; // login failed
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		 
		 bIsLoggedIn = true;
		 
		 // instantiate access to the submenus
		 cm = new CustomerMenu(this, connection, username, password);
		 am = new AdminMenu(this, connection, username, password);
		 return true;
	 }
	 
	 public void doMainMenu(Scanner sc) {
		 if(!bIsConnected) {
			 System.out.println("Error: Not connected to database system. Please connect to DBMS first.");
			 return;
		 }
		 
		 if(!bIsLoggedIn) {
			 System.out.println("Error: Not logged in. Please login first.");
			 return;
		 }
		 
		 if(bIsAdmin) {
			 am.doAdminMenu(sc);
		 }
		 else {
			 cm.doCustomerMenu(sc);
		 }
		
		 // User quit
		System.out.println("Bye " + username + "!");
	 }
	 
	/**
	 * This is a workaround. In a real system, you would not do this.
	 */
	private void fixHardcodedAuctions() {
		 // Get categories
		 HashMap<String,HashSet<String>> categories = new HashMap<String,HashSet<String>>();
		 // put parent categories in hashmap
		 try {
			 ResultSet rs = doQuery("select name from category where parent_category is null");
			 while(rs.next()) {
				 categories.put(rs.getString(1), new HashSet<String>());
			 }
			 rs = doQuery("select name,parent_category from category where parent_category is not null");
			 while(rs.next()) {
				 if(!categories.containsKey(rs.getString(2)))
					 categories.put(rs.getString(2), new HashSet<String>());
				 categories.get(rs.getString(2)).add(rs.getString(1));
			 }
			 rs = doQuery("select * from belongsto");
			 HashMap<Integer, String> belongsto = new HashMap<Integer, String>();
			 while(rs.next()) {
				 belongsto.put(rs.getInt(1), rs.getString(2));
			 }
			 
			 
			 for(Integer i : belongsto.keySet()) {
				 // not a root node
				 HashSet<String> addto = new HashSet<String>();
				 if(!categories.containsKey(belongsto.get(i))) {
					 for(String k : categories.keySet()) {
						 for(String c : categories.get(k)) {
							 if(c.equals(i) || addto.contains(c)) {
								 addto.add(k);
								 break;
							 }
						 }
					 }
				 }
				 for(String s : addto) {
					 doQuery("insert into belongsto (auction_id, category) values ("+i+", "+s+")");
					 connection.commit();
				 }
			 }
			 rs.close();
		} catch (Exception e) {
			System.out.println("SQL Error: " + e);
		}
	}
	 
	public static void main(String args[]) {
		String user = "";
		String pass = "";
		Scanner sc = new Scanner(System.in);
		System.out.println("CS 1555 Term Project - myAuction (Group 5)");
		System.out.println("Raymond Wang (Gengyu)");
		System.out.println("Ben Isenberg");
		System.out.println("---------------------------------------------");
		
		System.out.print("Enter username: ");
		user = sc.nextLine();
		System.out.print("Enter password: ");
		pass = sc.nextLine();
		
		MainMenu mm = new MainMenu(user, pass);
		
		System.out.println();
		
		if(!mm.doDBConnect()) {
			System.out.println("Error: Database connection failed. Please check server credentials.");
			sc.close();
			return;
		}
		
		// apply fix for hardcoded auctions from before
		mm.fixHardcodedAuctions();
		mm.fixHardcodedAuctions();
		
		if(mm.doLogin()) {
			System.out.println("Logged in as " + user + ". Welcome.");
			mm.doMainMenu(sc);
		}
		else {
			System.out.println("Error: user credentials invalid; login failed");
		}
		
		sc.close();
		mm.doDBDisconnect();
	}
}
