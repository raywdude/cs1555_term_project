/**
 * CS1555 Term Project - myAuction (Group 5)
 * by Raymond Wang (Gengyu), Ben Isenberg
 * gew11@pitt.edu, bji6@pitt.edu
 * 
 * Copyright (C) 2013, All rights reserved.
 * 
 *	Redistribution and use in source and binary forms, with or without
 *	modification, are permitted provided that the following conditions are met:
 *	    * Redistributions of source code must retain the above copyright
 *	      notice, this list of conditions and the following disclaimer.
 *	    * Redistributions in binary form must reproduce the above copyright
 *	      notice, this list of conditions and the following disclaimer in the
 *	      documentation and/or other materials provided with the distribution.
 *	    * Neither the name of the <organization> nor the
 *	      names of its contributors may be used to endorse or promote products
 *	      derived from this software without specific prior written permission.
 *	
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *	DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;


public class AdminMenu {
	 private Connection connection; //used to hold the jdbc connection to the DB
	 private String username, password;
	 
	 private MainMenu mm;
	 
	 public AdminMenu(MainMenu mm, Connection c, String u, String p) {
		 username = u;
		 password = p;
		 connection = c;
		 this.mm = mm;
	 }
	 
	 public void doAdminMenu(Scanner sc) {
		 int option = 0;
		 do {
			 // display menu
			 System.out.println("--------------ADMIN MENU------------");
			 System.out.println("1: New customer registration");
			 System.out.println("2: Update system date");
			 System.out.println("3: Show product statistics");
			 System.out.println("0: Logout and quit session");
			 System.out.println("------------------------------------");
			 
			 // prompt user for input
			 System.out.print("Enter your option: ");
			 option = sc.nextInt(); sc.nextLine();
			 
			 switch(option) {
			 case 1:
				 // new customer registration
				 doNewCustomer(sc);
				 break;
			 case 2:
				 // update system date
			 	 updateSystemTime(sc);
				 break;
			 case 3:
				 // show product statistics
			 	 statisticsMenu(sc);
				 break;
			 default: // everything else... ignore
				 break;
			 }
		 }
		 while(option != 0);
	 }
	 
	 public void doNewCustomer(Scanner sc) {
		 // Add new customer
		 String user = "", pass = "", name = "", address = "", email = "";
		 
		 System.out.print("Enter username: ");
		 user = sc.nextLine();
		 System.out.print("Enter password: ");
		 pass = sc.nextLine();
		 System.out.print("Enter full name: ");
		 name = sc.nextLine();
		 System.out.print("Enter address : ");
		 address = sc.nextLine();
		 System.out.print("Enter email: ");
		 email = sc.nextLine();
		 
		 // insert into CUSTOMER  values ('TheUser', 'Password', 'TheName', 'TheAddress', 'Email@WebSite.com');
		 try {
			 String query = "insert into CUSTOMER  values (?, ?, ?, ?, ?)";
			 PreparedStatement prep = connection.prepareStatement(query);
			 prep.setString(1, user);
			 prep.setString(2, pass);
			 prep.setString(3, name);
			 prep.setString(4, address);
			 prep.setString(5, email);
			 ResultSet res = prep.executeQuery();
			 if(res != null && res.next()) {
				 // commit changes
				 connection.commit();
				 System.out.println("User " + user + " successfully added!");
				 res.close();
			 }
			 else {
				 System.out.println("Error: Failed to add new user " + user + ".");
			 }

		 }catch(SQLException se) {
			 System.out.println("Error: Failed to add new user " + user + ". (Most likely user already exists)");
		 }
	 }


	 //Method to update the system time table
	 public void updateSystemTime(Scanner sc)
	 {
	 	String date = "";
	 	String time = "";

	 	System.out.print("Enter date (YYYY-MM-DD): ");
		date = sc.nextLine();
		System.out.print("Enter time (HH:MM:SS): ");
		time = sc.nextLine();
		System.out.print("Type 'A' for A.M. or 'P' for P.M.: ");

		String temp = sc.nextLine();
		
		if (temp.compareToIgnoreCase("A") == 0)
		{
			time += " A.M.";
		}
		else
		{
			time += " P.M.";
		}

		//Must be formatted to look like YYYY-MM-DD HH:MI:SS A.M.
		date += (" " + time);


		 // update OURDATE with the new date
		 try {
			 String query = "UPDATE OURDATE set OURDATE.C_date=TO_DATE('";
			 query += date;
			 query += "','YYYY-MM-DD HH:MI:SS P.M.')";
			
			 PreparedStatement prep = connection.prepareStatement(query);
			 
			 prep.executeUpdate();
			 
			 connection.commit();
			

		 }catch(SQLException se) {
			 System.out.println("Error: Failed to update System time.");
			 System.out.println(se.getMessage());
		 }

	 }


	 //Method that provides Admin's with a menu to chose what statistics they want to see
	 public void statisticsMenu(Scanner sc)
	 {
	 	
	 	int option = 0;
		
		do {
		 	// display product statistics menu
			System.out.println("--------------PRODUCT STATISTICS MENU------------");
			System.out.println("1: Product Report");
			System.out.println("2: Highest Volume Categories (only leaf categories)");
			System.out.println("3: Highest Volume Categories (only root categories)");
			System.out.println("4: Most Active Bidders");
			System.out.println("5: Most Active Buyers");			
			System.out.println("0: Go Back to Admin Menu");
			System.out.println("------------------------------------");

			// prompt user for input
			System.out.print("Enter your option: ");
			option = sc.nextInt(); 
			sc.nextLine();
			 
			switch(option) 
			{
				case 1:
					getProductReport(sc); //product report
					break;
				case 2:
					highestVolumeLeaf(sc); //highest volume leaf categories
					break;
				case 3:
					highestVolumeRoot(sc); //highest volume root categories
					break;
				case 4:
					getActiveBidders(sc); //most active bidders
					break;
				case 5:
					getActiveBuyers(sc); //most active buyers
					break;
				default:
					break;
			}
		
		} while (option != 0);

	 }


	//Method to query the database and display a product report
	 public void getProductReport(Scanner sc)
	 {
	 	System.out.print("Enter 1 for all products, Enter 2 to specify a seller: ");

	 	int choice = sc.nextInt();
	 	sc.nextLine(); //clear newline character from scanner buffer

	 	//Get report for all products 
	 	if (choice == 1)
	 	{
			try {
				 String query = "select PRODUCT.Name, PRODUCT.Status, PRODUCT.Amount, BIDLOG.Bidder from PRODUCT, BIDLOG where PRODUCT.Auction_id = BIDLOG.Auction_id and PRODUCT.Amount = BIDLOG.Amount";
				 
				 ResultSet results = mm.doQuery(query);
				 String fmtstring1 = "%20s %15s %7d %10s\n";
				 String fmtstring2 = "%20s %15s %7s %10s\n";

				 System.out.printf(fmtstring2, "Name", "Status", "Amount", "Bidder");
				 System.out.println("--------------------------------------------");

				 while (results.next())
				 {
				 	System.out.printf(fmtstring1, results.getString(1), results.getString(2), results.getInt(3), results.getString(4));
				 }
								

			}catch(Exception se) {
				 System.out.println("Error: Failed to retrieve product report.");
				 System.out.println(se.getMessage());
			}
	 	}
	 	else if (choice == 2) //Report for a specified seller
	 	{
	 		String customer;

	 		//Display a list of all current customers in database
	 		String get_customers = "select CUSTOMER.Login FROM CUSTOMER";

	 		//String formatting
		 	String fmtstring1 = "%10s\n";
		 	try
		 	{
		 		ResultSet results = mm.doQuery(get_customers);

				System.out.printf(fmtstring1, "Login");
				System.out.println("-----------");

				while (results.next())
				{
					System.out.printf(fmtstring1, results.getString(1));
				}
		 	}
		 	catch(Exception se) 
		 	{
				System.out.println("Error: Failed to display customers");
				System.out.println(se.getMessage());
		 	}	

		 	//Display product report for specified customer
	 		System.out.println("Enter which seller's report you would like to see:");
	 		customer = sc.nextLine();

	 		String query = "select PRODUCT.Name, PRODUCT.Status, PRODUCT.Amount, BIDLOG.Bidder from PRODUCT, BIDLOG where PRODUCT.Auction_id = BIDLOG.Auction_id and PRODUCT.Amount = BIDLOG.Amount and PRODUCT.Seller = '" + customer + "'";

	 		try
	 		{
	 			ResultSet results = mm.doQuery(query);
	 			fmtstring1 = "%20s %15s %7d %10s\n";
				String fmtstring2 = "%20s %15s %7s %10s\n";

				System.out.printf(fmtstring2, "Name", "Status", "Amount", "Bidder");
				System.out.println("----------------------------------------------");

				while (results.next())
				{
				 	System.out.printf(fmtstring1, results.getString(1), results.getString(2), results.getInt(3), results.getString(4));
				}


	 		}catch(Exception se) {
				 System.out.println("Error: Failed to retrieve product report.");
				 System.out.println(se.getMessage());
			}


	 	}



	 }


	 //Method to get top K highest volume leaf categories
	 public void highestVolumeLeaf(Scanner sc)
	 {
	 	int numMonths;
	 	int numRows;

	 	System.out.print("Enter the number of rows you would like to see: ");
	 	numRows = sc.nextInt();
	 	sc.nextLine();

	 	System.out.print("Enter how many months in the past to consider: ");
	 	numMonths = sc.nextInt();
	 	sc.nextLine();

	 	//Build queries
	 	String query1 = "create table Temp as select C.Name, Product_Count(C.Name, " + numMonths + ") as p_count from CATEGORY C where not exists (select null from CATEGORY where C.Name = CATEGORY.Parent_category) order by p_count DESC"; 

	 	String query2 = "select Temp.Name, Temp.p_count from Temp where rownum <= " + numRows;

	 	String query3 = "drop table Temp";
	 	String query4 = "purge recyclebin";

	 	try
	 	{
	 		//Create temporary table
	 		mm.doQuery(query1);
	 		connection.commit();

	 		//Select rows from table
	 		ResultSet results = mm.doQuery(query2);

	 		//String formatting
		 	String fmtstring1 = "%20s %6d\n";

	 		System.out.printf("%20s %6s\n", "Category", "Amount");
			System.out.println("-----------------------------------------");

			while (results.next())
			{
			 	System.out.printf(fmtstring1, results.getString(1), results.getInt(2));
			}

			//Drop temporary table
			mm.doQuery(query3);
			connection.commit();

			//Purge recyclebin
			mm.doQuery(query4);
			connection.commit();

	 	}
	 	catch(Exception se) {
			System.out.println("Error: Failed to retrieve statistics.");
			System.out.println(se.getMessage());
	 	}

	 }


	 //Method to get top K highest volume root categories
	 public void highestVolumeRoot(Scanner sc)
	 {
	 	int numMonths;
	 	int numRows;

	 	System.out.print("Enter the number of rows you would like to see: ");
	 	numRows = sc.nextInt();
	 	sc.nextLine();

	 	System.out.print("Enter how many months in the past to consider: ");
	 	numMonths = sc.nextInt();
	 	sc.nextLine();

	 	//Build queries
	 	String query1 = "create table Temp as select CATEGORY.Name, Product_Count(CATEGORY.Name, " + numMonths + ") as p_count from CATEGORY where CATEGORY.Parent_category IS NULL order by p_count DESC"; 

	 	String query2 = "select Temp.Name, Temp.p_count from Temp where rownum <= " + numRows;

	 	String query3 = "drop table Temp";
	 	String query4 = "purge recyclebin";

	 	try
	 	{
	 		//Create temporary table
	 		mm.doQuery(query1);
	 		connection.commit();

	 		//Select rows from table
	 		ResultSet results = mm.doQuery(query2);

	 		//String formatting
	 		String fmtstring = "%20s %6d\n";

	 		System.out.printf("%20s %6s\n", "Category", "Amount");
			System.out.println("-----------------------------------");

			while (results.next())
			{
			 	System.out.printf(fmtstring, results.getString(1), results.getInt(2));
			}

			//Drop temporary table
			mm.doQuery(query3);
			connection.commit();

			//Purge recyclebin
			mm.doQuery(query4);
			connection.commit();

	 	}
	 	catch(Exception se) {
			System.out.println("Error: Failed to retrieve statistics.");
			System.out.println(se.getMessage());
	 	}
	 	
	 }


	 //Method to get top K most active bidders
	 public void getActiveBidders(Scanner sc)
	 {
	 	int numMonths;
	 	int numRows;

	 	System.out.print("Enter the number of rows you would like to see: ");
	 	numRows = sc.nextInt();
	 	sc.nextLine();

	 	System.out.print("Enter how many months in the past to consider: ");
	 	numMonths = sc.nextInt();
	 	sc.nextLine();

	 	//Build queries
	 	String query1 = "create table Temp as select distinct BIDLOG.Bidder, Bid_Count(BIDLOG.Bidder, " + numMonths + ") as Bid_Amount from BIDLOG order by Bid_Amount DESC"; 

	 	String query2 = "select Temp.Bidder from Temp where rownum <= " + numRows;

	 	String query3 = "drop table Temp";
	 	String query4 = "purge recyclebin";

	 	try
	 	{
	 		//Create temporary table
	 		mm.doQuery(query1);
	 		connection.commit();

	 		//Select rows from table
	 		ResultSet results = mm.doQuery(query2);

	 		System.out.printf("%10s\n", "Bidder");
			System.out.println("-----------------------");

			while (results.next())
			{
			 	System.out.printf("%10s\n", results.getString(1));
			}

			//Drop temporary table
			mm.doQuery(query3);
			connection.commit();

			//Purge recyclebin
			mm.doQuery(query4);
			connection.commit();

	 	}
	 	catch(Exception se) {
			System.out.println("Error: Failed to retrieve most active bidders.");
			System.out.println(se.getMessage());
	 	}
	 }


	 //Method to get top K most active buyers
	 public void getActiveBuyers(Scanner sc)
	 {
	 	int numMonths;
	 	int numRows;

	 	System.out.print("Enter the number of rows you would like to see: ");
	 	numRows = sc.nextInt();
	 	sc.nextLine();

	 	System.out.print("Enter how many months in the past to consider: ");
	 	numMonths = sc.nextInt();
	 	sc.nextLine();

	 	//Build  SQL queries
	 	String query1 = "create table Temp as select distinct BIDLOG.Bidder, Buying_Amount(BIDLOG.Bidder, " + numMonths + ") as Amount_spent from BIDLOG order by Amount_spent DESC"; 

	 	String query2 = "select Temp.Bidder from Temp where rownum <= " + numRows;

	 	String query3 = "drop table Temp";
	 	String query4 = "purge recyclebin";

	 	try
	 	{
	 		//Create temporary table
	 		mm.doQuery(query1);
	 		connection.commit();

	 		//Select rows from table
	 		ResultSet results = mm.doQuery(query2);

	 		System.out.printf("%10s\n", "Buyer");
			System.out.println("-----------------------");

			while (results.next())
			{
			 	System.out.printf("%10s\n", results.getString(1));
			}

			//Drop temporary table
			mm.doQuery(query3);
			connection.commit();

			//Purge recyclebin
			mm.doQuery(query4);
			connection.commit();

	 	}
	 	catch(Exception se) {
			System.out.println("Error: Failed to retrieve most active buyers.");
			System.out.println(se.getMessage());
	 	}
	 }
}
