--Names: Ben Isenberg, Raymond Wang (Gengyu) 4/3/2013
--Usernames: BJI6, GEW11

-- 2a Browsing Products
select * from (product p join belongsto bt on p.auction_id=bt.auction_id) where bt.category='Books' and p.status='underauction';

-- 2b Search for product by text
select * from product where DESCRIPTION like '%keyword1%' and DESCRIPTION like '%keyword2%';

-- 2c Putting products for auction
create or replace procedure put_product (seller IN varchar2, name IN varchar2, description IN varchar2, category_p IN varchar2, numdays IN integer, auctionid OUT integer)
as
	startdate date;
	validcat number;
begin
	-- check Category(name, parent_category) for valid entry passed in "category" parameter
	select count(*) into validcat from category where name like '%' || category_p || '%';
	
	-- category check fail
	if validcat <= 0 then
		auctionid:= -1;
		return;
	end if;

	-- initialize
	auctionid := 0;
	startdate := TO_DATE('2012-01-01 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.');
	-- select the highest if possible
	select max(auction_id) into auctionid from product;
	-- increment it
	auctionid := auctionid + 1;
	-- get date from the OURDATE table (NEEDS OURDATE TABLE TO EXIST BEFORE THIS PROCEDURE WILL WORK)
	select C_date into startdate from OURDATE;
	
	-- need to insert into product and BelongsTo(auction_id, category)
	insert into product (auction_id, name, description, buyer, seller, start_date, number_of_days, status)
			values(auctionid, name, description, seller, seller, startdate, numdays, 'underauction');
	insert into belongsto (auction_id, category) values (auctionid, category_p);

	return;
end put_product;
/
show errors;


-- 2f Suggestions
select * from (product prodz join bidlog logz on prodz.auction_id=logz.auction_id)
where logz.bidder in 
(select bidder from bidlog log2 
where log2.bidder<>'TheUserID' and log2.auction_id in 
(select prod.auction_id as auction_id from (product prod join bidlog loga on loga.auction_id=prod.auction_id)
where loga.bidder='TheUserID' and prod.status='underauction'));

-- 3a New customer registration
insert into CUSTOMER  values ('TheUser', 'Password', 'TheName', 'TheAddress', 'Email@WebSite.com');

-- 4a Write the SQL statement that will return the information for a user, given a login
name and password. If the login name + password does not match any entry in
the database the system returns an empty list.

select * from ADMINISTRATOR where LOGIN='TheUser' and PASSWORD='Password'
union
select * from CUSTOMER where LOGIN='TheUser' and PASSWORD='Password';