drop table category cascade constraints;
drop table product cascade constraints;
drop table customer cascade constraints;
drop table administrator cascade constraints;
drop table ourdate cascade constraints;

drop table bidlog cascade constraints;

drop table belongsto cascade constraints;
