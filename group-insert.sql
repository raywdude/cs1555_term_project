--Names: Ben Isenberg, Raymond Wang (Gengyu) 3/30/2013
--Usernames: BJI6, GEW11


--Insert sample data
INSERT INTO OURDATE
VALUES ( TO_DATE('2012-12-11 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.') );

INSERT INTO ADMINISTRATOR
VALUES ('admin', 'root', 'adminstrator', '6810 SENSQ', 'admin@1555.com');

INSERT INTO CUSTOMER
VALUES ('user0', 'pwd', 'user0', '6810 SENSQ', 'user0@1555.com');

INSERT INTO CUSTOMER
VALUES ('user1', 'pwd', 'user1', '6811 SENSQ', 'user1@1555.com');

INSERT INTO CUSTOMER
VALUES ('user2', 'pwd', 'user2', '6812 SENSQ', 'user2@1555.com');

INSERT INTO CUSTOMER
VALUES ('user3', 'pwd', 'user3', '6813 SENSQ', 'user3@1555.com');

INSERT INTO CUSTOMER
VALUES ('user4', 'pwd', 'user4', '6814 SENSQ', 'user4@1555.com');

--I had to use Oracle's TO_DATE function to insert time information into a DATE type
INSERT INTO PRODUCT
VALUES (0, 'Database', 'SQL ER-design', 'user0', TO_DATE('2012-12-04 12:00:00 A.M.','YYYY-MM-DD HH:MI:SS A.M.'), 50, 2, 'sold', 'user2', TO_DATE('2012-12-06 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 53);

INSERT INTO PRODUCT
VALUES (1, '17 inch monitor', '17 inch monitor', 'user0', TO_DATE('2012-12-06 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 100, 2, 'sold', 'user4', TO_DATE('2012-12-08 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 110);

INSERT INTO PRODUCT (Auction_id, Name, Description, Seller, Start_date, Min_price, Number_of_days, Status)
VALUES (2, 'DELL INSPIRON 1100', 'DELL INSPIRON notebook', 'user0', TO_DATE('2012-12-07 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 500, 7, 'underauction');

INSERT INTO PRODUCT
VALUES (3, 'Return of the King', 'fantasy', 'user1', TO_DATE('2012-12-07 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40, 2, 'sold', 'user2', TO_DATE('2012-12-09 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO PRODUCT
VALUES (4, 'The Sorcerer Stone', 'Harry Potter series', 'user1', TO_DATE('2012-12-08 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40, 2, 'sold', 'user3', TO_DATE('2012-12-10 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO PRODUCT (Auction_id, Name, Description, Seller, Start_date, Min_price, Number_of_days, Status)
VALUES (5, 'DELL INSPIRON 1100', 'DELL INSPIRON notebook', 'user1', TO_DATE('2012-12-09 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 200, 1, 'withdrawn');

INSERT INTO PRODUCT (Auction_id, Name, Description, Seller, Start_date, Min_price, Number_of_days, Status, Amount)
VALUES (6, 'Advanced Database', 'SQL Transaction index', 'user1', TO_DATE('2012-12-10 12:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 50, 2, 'underauction', 55);

INSERT INTO BIDLOG
VALUES (0, 0, 'user2', TO_DATE('2012-12-04 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 50);

INSERT INTO BIDLOG
VALUES (1, 0, 'user3', TO_DATE('2012-12-04 09:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 53);

INSERT INTO BIDLOG
VALUES (2, 0, 'user2', TO_DATE('2012-12-05 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 60);

INSERT INTO BIDLOG
VALUES (3, 1, 'user4', TO_DATE('2012-12-06 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 100);

INSERT INTO BIDLOG
VALUES (4, 1, 'user2', TO_DATE('2012-12-07 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 110);

INSERT INTO BIDLOG
VALUES (5, 1, 'user4', TO_DATE('2012-12-07 09:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 120);

INSERT INTO BIDLOG
VALUES (6, 3, 'user2', TO_DATE('2012-12-07 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO BIDLOG
VALUES (7, 4, 'user3', TO_DATE('2012-12-09 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 40);

INSERT INTO BIDLOG
VALUES (8, 6, 'user2', TO_DATE('2012-12-07 08:00:00 A.M.', 'YYYY-MM-DD HH:MI:SS A.M.'), 55);

INSERT INTO CATEGORY (Name)
VALUES ('Books');

INSERT INTO CATEGORY
VALUES ('Textbooks', 'Books');

INSERT INTO CATEGORY
VALUES ('Fiction books', 'Books');

INSERT INTO CATEGORY
VALUES ('Magazines', 'Books');

INSERT INTO CATEGORY
VALUES ('Computer Science', 'Textbooks');

INSERT INTO CATEGORY
VALUES ('Math', 'Textbooks');

INSERT INTO CATEGORY
VALUES ('Philosophy', 'Textbooks');

INSERT INTO CATEGORY (Name)
VALUES ('Computer Related');

INSERT INTO CATEGORY
VALUES ('Desktop PCs', 'Computer Related');

INSERT INTO CATEGORY
VALUES ('Laptops', 'Computer Related');

INSERT INTO CATEGORY
VALUES ('Monitors', 'Computer Related');

INSERT INTO CATEGORY
VALUES ('Computer books', 'Computer Related');

INSERT INTO BELONGSTO
VALUES (0, 'Computer Science');

INSERT INTO BELONGSTO
VALUES (0, 'Computer books');

INSERT INTO BELONGSTO
VALUES (1, 'Monitors');

INSERT INTO BELONGSTO
VALUES (2, 'Laptops');

INSERT INTO BELONGSTO
VALUES (3, 'Fiction books');

INSERT INTO BELONGSTO
VALUES (4, 'Fiction books');

INSERT INTO BELONGSTO
VALUES (5, 'Laptops');

INSERT INTO BELONGSTO
VALUES (6, 'Computer Science');

INSERT INTO BELONGSTO
VALUES (6, 'Computer books');


