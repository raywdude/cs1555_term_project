--Names: Ben Isenberg, Raymond Wang (Gengyu) 3/30/2013
--Usernames: BJI6, GEW11

--Empty recycle bin
PURGE RECYCLEBIN;

--Table keeps track of time for auction system
create table OURDATE
(
    C_date date
);

create table CUSTOMER
(
    Login varchar2(10) not null,
    Password varchar2(10),
    Name varchar2(20),
    Address varchar2(30),
    Email varchar2(20),
    --Primary Key
    constraint Customer_PK PRIMARY KEY(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table ADMINISTRATOR
(
    Login varchar2(10) not null,
    Password varchar2(10),
    Name varchar2(20),
    Address varchar2(30),
    Email varchar2(20),
    --Primary Key
    constraint Administrator_PK PRIMARY KEY(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table PRODUCT
(
    Auction_id int not null,
    Name varchar2(20),
    Description varchar2(30),
    Seller varchar2(10),
    Start_date date,
    Min_price int,    
    Number_of_days int,
    Status varchar2(15) not null,
    Buyer varchar2(10),
    Sell_date date,
    Amount int,
    --Primary Key
    constraint Product_PK PRIMARY KEY(Auction_id) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Keys
    constraint Product_FK1 FOREIGN KEY(Seller) references CUSTOMER(Login) DEFERRABLE INITIALLY IMMEDIATE,
    constraint Product_FK2 FOREIGN KEY(Buyer) references CUSTOMER(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table BIDLOG
(
    Bidsn int not null,
    Auction_id int,
    Bidder varchar2(10),
    Bid_time date,
    Amount int,
    --Primary Key
    constraint Bidlog_PK PRIMARY KEY(Bidsn) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Keys
    constraint Bidlog_FK1 FOREIGN KEY(Auction_id) references PRODUCT(Auction_id) DEFERRABLE INITIALLY IMMEDIATE,
    constraint Bidlog_FK2 FOREIGN KEY(Bidder) references CUSTOMER(Login) DEFERRABLE INITIALLY IMMEDIATE
);

create table CATEGORY
(
    Name varchar2(20) not null,
    Parent_category varchar2(20),
    --Primary Key
    constraint Category_PK PRIMARY KEY(Name) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Key
    constraint Category_FK FOREIGN KEY(Parent_category) references CATEGORY(Name) DEFERRABLE INITIALLY IMMEDIATE
);

create table BELONGSTO
(
    Auction_id int not null,
    Category varchar2(20) not null,
    --Primary key
    constraint Belongsto_PK PRIMARY KEY(Auction_id, Category) DEFERRABLE INITIALLY IMMEDIATE,
    --Foreign Key
    constraint Belongsto_FK1 FOREIGN KEY(Auction_id) references PRODUCT(Auction_id) DEFERRABLE INITIALLY IMMEDIATE,
    constraint Belongsto_FK2 FOREIGN KEY(Category) references CATEGORY(Name) DEFERRABLE INITIALLY IMMEDIATE
);





















    
